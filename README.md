This is obs-studio compiled with a few extra add-ons:

(1) browser source plugin:
https://github.com/obsproject/obs-browser

(2) hevc-vaapi encoder support:
https://github.com/obsproject/obs-studio/pull/2246

(3) game-capture plugin:
https://github.com/nowrep/obs-vkcapture/

Note, you -will- need additional packages from here for game-capture to work:

https://copr.fedorainfracloud.org/coprs/gloriouseggroll/obs-studio-gamecapture/

If you do not want to build this, I host a fedora-compatible repository for it here:

`sudo vim /etc/yum.repos.d/obs-studio-nobara.repo`

```
[nobara-obs-studio]
name=nobara-obs-studio
#baseurl=https://www.nobaraproject.org/repo/fedora/$releasever/$basearch/obs-studio-nobara
mirrorlist=https://www.nobaraproject.org/mirrorlist-nobara-obs-studio
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://www.nobaraproject.org/repo/nobara/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
```

Then update with:

`sudo dnf update obs-studio --refresh`

or install with:

`sudo dnf install obs-studio`


Build instructions:
```
$ sudo dnf install mock pykickstart
$ sudo usermod -a -G mock <user>
$ mock -r /etc/mock --init
$ sudo setenforce 0
$ mock -r nobara-36-x86_64.cfg --rebuild --enable-network *.src.rpm
$ mv /var/lib/mock/nobara-36-x86_64/result/*.rpm /path/to/some/folder/
$ exit
$ mock -r ./nobara-36-x86_64.cfg --scrub=all
$ sudo setenforce 1
```
Finished! Packages will be in whatever folder you chose for /path/to/some/folder/

Install instructions:
```
$ cd /path/to/some/folder/
$ yum install obs-studio-2*.rpm
```

Upgrade instructions:
```
$ cd /path/to/some/folder/
$ yum upgrade *.rpm
```

IMPORTANT NOTES:
- Game capture does -not- work without the additional obs-studio-gamecapture package. You will need this:

https://copr.fedorainfracloud.org/coprs/gloriouseggroll/obs-studio-gamecapture/

- Once the gamecapture packages have been installed, you can capture most games using:

`obs-gamecapture somegame`

For example with steam games in the launch options for the game:

`obs-gamecapture %COMMAND%`

Then add 'Game Capture' as a source in OBS.

This works for vulkan + opengl and 64 + 32 bit games.


## Known Issues

* The vulkan capture -only- works on AMD due to Nvidia not having VK_EXT_external_memory_dma_buf in their drivers.

## Troubleshooting

**Cannot create EGLImage: Arguments are inconsistent**

If you get this error with Vulkan capture, try starting the game with `OBS_VKCAPTURE_LINEAR=1` environment variable.

**No Game Capture source available in OBS**

If you are on X11, make sure you run OBS with EGL enabled: `OBS_USE_EGL=1 obs`.
